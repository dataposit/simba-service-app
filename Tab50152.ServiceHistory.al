// tbl 50151
// tblext 50152
table 50152 "Service Entries (Hisorical)"
{
    DataClassification = CustomerContent;


    fields
    {
        field(1; "VIN"; Code[20])
        {
            Caption = 'VIN';
        }
        field(2; "Company"; Text[50])
        {
            Caption = 'Company';
        }
        field(3; "Site"; Text[50])
        {
            Caption = 'Site';
        }
        field(4; "Section"; Text[50])
        {
            Caption = 'Section';
        }
        field(5; "Department"; Text[50])
        {
            Caption = 'Department';
        }
        field(6; "WO"; Code[20])
        {
            Caption = 'WO';
        }
        field(7; "Registration"; Code[20])
        {
            Caption = 'Registration';
            NotBlank = false;
        }
        field(8; "Invoice Dt."; Date)
        {
            Caption = 'Invoice Dt.';
        }
        field(9; "Delivery Dt."; Date)
        {
            Caption = 'Delivery Dt.';
        }
        field(10; "Brand"; Code[20])
        {
            Caption = 'Brand';
        }
        field(11; "Range"; Code[20])
        {
            Caption = 'Range';
        }
        field(12; "Invoice No."; Code[20])
        {
            Caption = 'Invoice No.';
        }
        field(13; "Invoice Customer"; Text[100])
        {
            Caption = 'Invoice Customer';
        }
        field(14; "Charge Type"; Text[50])
        {
            Caption = 'Charge Type';
        }
        field(15; "Days Open"; Integer)
        {
            Caption = 'Days Open';
        }
        field(16; "Creation Employee"; Text[100])
        {
            Caption = 'Creation Employee';
        }
        field(17; "Service Adviser Responsible"; Text[100])
        {
            Caption = 'Service Adviser Responsible';
        }
        field(18; "Closure employee"; Text[100])
        {
            Caption = 'Closure employee';
        }
        field(19; "Delivery Service Adviser"; Text[100])
        {
            Caption = 'Delivery Service Adviser';
        }
        field(20; "INVOICE REF"; Code[35])
        {
            Caption = 'INVOICE REF';
        }
        field(21; "WORK ORDER NO"; Code[35])
        {
            Caption = 'WORK ORDER NO';
        }
        field(22; "JOB NO"; Integer)
        {
            Caption = 'JOB NO';
        }
        field(23; "RESOURCE/ITEM"; Code[50])
        {
            Caption = 'RESOURCE/ITEM';
        }
        field(24; "Reference"; Code[50])
        {
            Caption = 'Reference';
        }
        field(25; "Description"; Text[300])
        {
            Caption = 'Description';
        }
        field(26; "Quantity"; Integer)
        {
            Caption = 'Quantity';
        }
        field(27; "Unit"; Text[50])
        {
            Caption = 'Unit';
            NotBlank = false;
        }
        field(28; "Price"; Decimal)
        {
            Caption = 'Price';
        }
        field(29; "% Disc."; Decimal)
        {
            Caption = '% Disc.';
        }
        field(30; "Amount"; Decimal)
        {
            Caption = 'Amount';
        }
        field(31; "JOB DESCRIPTION"; Text[300])
        {
            Caption = 'JOB DESCRIPTION';
        }
        field(32; "Imputation type"; Text[100])
        {
            Caption = 'Imputation type';
        }
        field(33; "Cost"; Decimal)
        {
            Caption = 'Cost';
        }
        field(34; "Third Party Number"; Code[20])
        {
            Caption = 'Third Party Number';
        }
        field(35; "TotalCost"; Decimal)
        {
            Caption = 'TotalCost';
        }
        field(36; IDN; Integer)
        {
            Caption = 'IDN';
        }
        field(37; Mileage; Integer)
        {
            Caption = 'MILEAGE';
        }
    }

    keys
    {
        key(PK; IDN)
        {
            Clustered = true;
        }
        key(Key1; "Invoice Dt.", "Invoice No.")
        {

        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin
        //Error('Insert of new records disabled');
    end;

    trigger OnModify()
    begin
        //Error('Modification of records disabled');
    end;

    trigger OnDelete()
    begin
        //Error('Deletion of records disabled');
    end;

    trigger OnRename()
    begin
        Error('Renaming of records disabled');
    end;

}