// pg 50152
// pgext 50152
page 50154 "Service Entries (Hisorical)"
{
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "Service Entries (Hisorical)";
    Editable = false;
    Caption = 'Service Ledger Entries (Historical)';

    layout
    {
        area(Content)
        {
            repeater(Control1)
            {
                field("INVOICE REF"; "INVOICE REF")
                {
                    Caption = 'INVOICE REF';
                    ApplicationArea = all;
                }
                field("WORK ORDER NO"; "WORK ORDER NO")
                {
                    Caption = 'WORK ORDER NO';
                    ApplicationArea = all;
                }
                field("JOB DESCRIPTION"; "JOB DESCRIPTION")
                {
                    Caption = 'JOB DESCRIPTION';
                    ApplicationArea = all;
                }
                field("Invoice Dt."; "Invoice Dt.")
                {
                    Caption = 'Invoice Dt.';
                    ApplicationArea = all;
                }
                field("Delivery Dt."; "Delivery Dt.")
                {
                    Caption = 'Delivery Dt.';
                    ApplicationArea = all;
                }
                field("JOB NO"; "JOB NO")
                {
                    Caption = 'JOB NO';
                    ApplicationArea = all;
                }
                field("RESOURCE/ITEM"; "RESOURCE/ITEM")
                {
                    Caption = 'RESOURCE/ITEM';
                    ApplicationArea = all;
                }
                field("Reference"; "Reference")
                {
                    Caption = 'Reference';
                    ApplicationArea = all;
                }
                field("Description"; "Description")
                {
                    Caption = 'Description';
                    ApplicationArea = all;
                }
                field("Quantity"; "Quantity")
                {
                    Caption = 'Quantity';
                    ApplicationArea = all;
                }
                field("Unit"; "Unit")
                {
                    Caption = 'Unit';
                    ApplicationArea = all;
                }
                field("Price"; "Price")
                {
                    Caption = 'Price';
                    ApplicationArea = all;
                }
                field("% Disc."; "% Disc.")
                {
                    Caption = '% Disc.';
                    ApplicationArea = all;
                }
                field("Amount"; "Amount")
                {
                    Caption = 'Amount';
                    ApplicationArea = all;
                }
                field("Brand"; "Brand")
                {
                    Caption = 'Brand';
                    ApplicationArea = all;
                }
                field("Range"; "Range")
                {
                    Caption = 'Range';
                    ApplicationArea = all;
                }
                field("Invoice No."; "Invoice No.")
                {
                    Caption = 'Invoice No.';
                    ApplicationArea = all;
                }
                field("Registration"; "Registration")
                {
                    Caption = 'Registration';
                    ApplicationArea = all;
                }
                field("Invoice Customer"; "Invoice Customer")
                {
                    Caption = 'Invoice Customer';
                    ApplicationArea = all;
                }
                field("Charge Type"; "Charge Type")
                {
                    Caption = 'Charge Type';
                    ApplicationArea = all;
                }
                field("Site"; "Site")
                {
                    Caption = 'Site';
                    ApplicationArea = all;
                }
                field("Section"; "Section")
                {
                    Caption = 'Section';
                    ApplicationArea = all;
                }
                field("Department"; "Department")
                {
                    Caption = 'Department';
                    ApplicationArea = all;
                }
                field("WO"; "WO")
                {
                    Caption = 'WO';
                    ApplicationArea = all;
                }
                field("Creation Employee"; "Creation Employee")
                {
                    Caption = 'Creation Employee';
                    ApplicationArea = all;
                }
                field("Service Adviser Responsible"; "Service Adviser Responsible")
                {
                    Caption = 'Service Adviser Responsible';
                    ApplicationArea = all;
                }
                field("Closure employee"; "Closure employee")
                {
                    Caption = 'Closure employee';
                    ApplicationArea = all;
                }
                field("Delivery Service Adviser"; "Delivery Service Adviser")
                {
                    Caption = 'Delivery Service Adviser';
                    ApplicationArea = all;
                }
                field(Mileage; Mileage)
                {
                    Caption = 'MILEAGE';
                    ApplicationArea = all;
                }

            }
        }
        area(Factboxes)
        {

        }
    }
}