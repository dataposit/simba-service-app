tableextension 50153 "Item Ext" extends item
{
    fields
    {
        field(50100; "Spiga Last Purchase Date"; Date)
        {
            Caption = 'Spiga Last Purchase Date';
            DataClassification = ToBeClassified;
        }
        field(50101; "Spiga Last Sales Date"; Date)
        {
            Caption = 'Spiga Last Sales Date';
            DataClassification = ToBeClassified;
        }
        field(50102; "Spiga Entry DT"; Date)
        {
            Caption = 'Spiga Entry DT';
            DataClassification = ToBeClassified;
        }

    }
}
