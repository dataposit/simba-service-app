pageextension 50153 "Vehicle Card Ext" extends "Vehicle Card"
{
    actions
    {
        // Add changes to page actions here
        addafter("<Action40>")
        {
            action("<Action41>")
            {
                ApplicationArea = Basic;
                Caption = 'Service Ledger E&ntries (Historical)';
                Image = ServiceLedger;
                RunObject = Page "Service Entries (Hisorical)";
                RunPageLink = "VIN" = field(VIN);
                RunPageView = sorting("Invoice Dt.");

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        myInt: Integer;
}