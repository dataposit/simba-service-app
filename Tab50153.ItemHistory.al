table 50153 "Item Ledger History"
{
    Caption = 'Item History';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Branch; Text[50])
        {
            Caption = 'Branch';
            DataClassification = ToBeClassified;
        }
        field(2; "Posting Date"; Date)
        {
            Caption = 'Posting Date';
            DataClassification = ToBeClassified;
        }
        field(3; "Document Date"; Date)
        {
            Caption = 'Document Date';
            DataClassification = ToBeClassified;
        }
        field(4; "Entry Type"; Text[50])
        {
            Caption = 'Entry Type';
            DataClassification = ToBeClassified;
        }
        field(5; Brand; Code[20])
        {
            Caption = 'Brand';
            DataClassification = ToBeClassified;
        }
        field(6; "Item No"; Code[40])
        {
            Caption = 'Item No';
            DataClassification = ToBeClassified;
        }
        field(7; Description; Text[100])
        {
            Caption = 'Description';
            DataClassification = ToBeClassified;
        }
        field(8; "Unit Cost"; Decimal)
        {
            Caption = 'Unit Cost';
            DataClassification = ToBeClassified;
        }
        field(9; Qty; Decimal)
        {
            Caption = 'Qty';
            DataClassification = ToBeClassified;
        }
        field(10; Location; Text[100])
        {
            Caption = 'Location';
            DataClassification = ToBeClassified;
        }
        field(11; Bin; Text[50])
        {
            Caption = 'Bin';
            DataClassification = ToBeClassified;
        }
        field(12; LineNo; Integer)
        {
            AutoIncrement = true;
        }
    }
    keys
    {
        key(PK; LineNo)
        {
            Clustered = true;
        }
        key(Key1; "Item No", "Posting Date")
        {

        }
    }

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}
