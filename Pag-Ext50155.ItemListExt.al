pageextension 50155 "Item List Ext" extends "Item List"
{
    layout
    {
        addafter(Description)
        {
            field("Spiga Last Purchase Date"; "Spiga Last Purchase Date")
            {
                Editable = false;
            }
            field("Spiga Last Sales Date"; "Spiga Last Sales Date")
            {
                Editable = false;
            }
        }
    }
}
