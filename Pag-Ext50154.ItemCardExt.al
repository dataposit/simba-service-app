pageextension 50154 "Item Card Ext" extends "Item Card"
{
    actions
    {
        // Add changes to page actions here
        addafter("Ledger E&ntries")
        {
            action("Ledger Entries History")
            {
                ApplicationArea = Basic;
                Caption = 'Item Ledger Ent&ries (Historical)';
                Image = ItemLedger;
                RunObject = Page "Item Ledger Entry(Historical)";
                RunPageLink = "Item No" = field("No.");
                RunPageView = sorting("Item No", "Posting Date");

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        myInt: Integer;
}
